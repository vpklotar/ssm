# DynMon - Synchronized Syslog Manager #

This project is all about managing and distributing logs. Missing functionality? Write a simple plug-in (see [Simple client handler](https://bitbucket.org/vpklotar/ssm/wiki/Client%20handler))

### Functionality ###
* Monitor file and send its data to a reciver
* Relay messages to multiple locations
* Send and recive encrypted messages using OpenSSL
* SystemD (journalctl) integration
* Garanteed delivery of syslog (TCP-Sender and file-buffering)

### Future plans ###
* Syslog integration

### What is this repository for? ###
This repository is hosting the DynMon - SSM client and server code.

### How do I get set up? ###
* Install python3
* Make sure the first line in both the SSMClient.py and SSMServer.py files (server and client) contain the correct path
* Make sure the Main.py files are runnable (chmod +x [SSMClient.py|SSMServer.py])
* Configure your ports and IP adresses (config.json)
* Start the application via ./SSMClient or ./SSMServer

### Configuration examples ###
See the [Wiki](https://bitbucket.org/vpklotar/ssm/wiki/Home).

### Contribution guidelines ###
Code review or simply more functionality is always appreciated!

### Who do I talk to? ###
* Repo owner or admin
* Other community or team contact