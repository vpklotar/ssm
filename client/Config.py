"""This is the config class, it handles all things to do with configuration."""
import json


class Config(object):
    """This class handles all the things has to do with configuration."""

    def __init__(self, config, main):
        """Initialize config."""
        if isinstance(config, str):
            self.cfg = json.loads(config)
        else:
            self.cfg = config
        self.main = main

    def __getitem__(self, key):
        """Get a config value."""
        try:
            return self.cfg[key]
        except KeyError:
            self.main.log("Tried to get " + str(key) + " from config but not \
                           such value exist")
            self.main.log("Now exiting")
            exit()

    def get_config(self, key):
        """Will return a new configuration object based on the key."""
        return Config(self.cfg[key], self.main)

    def get_default(self, key, default):
        """Will check if a key exist and return its value or the default."""
        if key not in self.cfg:
            return default
        else:
            return self.cfg[key]

    def get_required(self, key, message):
        """Will return a value if a key exists, otherwise kill the thread."""
        if key not in self.cfg:
            # Kill this thread as a required property was not set
            self.main.log(message)
            exit()
        else:
            return self.cfg[key]
