"""A helper module to handle communication with TCP."""

import socket
import queue
import threading
import time


class TCPHelper(object):
    """This class will handle all TCP communication (client)."""

    send_thread = None

    def __init__(self, address, port, main):
        """Initialize and setup the TCP-Helper."""
        self.address = address
        self.port = port
        self.main = main

        self.queue = queue.Queue()
        self.start_connection_thread()

    def start_connection_thread(self):
        """Start the thread responsible for the connection."""
        self.connection_thread = threading.Thread(target=self.conn_status)
        self.connection_thread.daemon = True
        self.connection_thread.start()

    def start_send_thread(self):
        """Start the thread responsible for sending messages."""
        self.send_thread = threading.Thread(target=self.send_thread_function)
        self.send_thread.daemon = True
        self.send_thread.start()

    def is_alive(self):
        """Will return if the sending_thread is alive."""
        return self.send_thread.isAlive()

    def send(self, data):
        """Will enqueue data to be sent."""
        self.queue.put(data)

    def conn_status(self):
        """Handle the connection status."""
        while True:
            try:
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.connect((self.address, self.port))
                self.main.log('Connection (' + self.address + ':' +
                              str(self.port) + ') established.')

                # The connection was established, break this loop
                self.start_send_thread()
                break
            except:
                self.main.log('Error connecting (' + self.address + ':' +
                              str(self.port) + ').')
                self.main.log('Trying again in 5 seconds.')
                time.sleep(5)

    def send_thread_function(self):
        """Thread function to handle all sending."""
        while True:
            data = self.queue.get(True)
            try:
                self.socket.sendto(data, (self.address, self.port))
            except:
                self.main.log("Something went wrong when sending data\
                              trying to reconnect.")
                self.start_connection_thread()
                break
