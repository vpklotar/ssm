"""A helper module to handle communication with UDP."""
import socket
import queue
import threading


class UDPHelper(object):
    """This class will handle all UDP communication (client)."""

    def __init__(self, address, port, main):
        """Initialize and setup UDP-Helper."""
        self.address = address
        self.port = port
        self.main = main

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

        self.queue = queue.Queue()

        # Start the thread responsible for sending
        self.send_thread = threading.Thread(target=self.send_thread_function)
        self.send_thread.daemon = True
        self.send_thread.start()

    def send(self, data):
        """Will enqueue data to be sent."""
        self.queue.put(data)

    def send_thread_function(self):
        """Thread function to handle all sending."""
        while True:
            data = self.queue.get(True)
            self.socket.sendto(data, (self.address, self.port))
