"""Base monitor."""


class ClientMonitor(object):
    """Base monitor to build all other monitors uppon."""

    def __init__(self, main, config):
        """Initialize and setup the monitor."""
        print("__init__ not implemented.")

    def load_properties(self):
        """Load all the properties needed."""
        print("load_properties not implemented.")

    def start(self):
        """Start the monitoring."""
        print("start not implemented.")
