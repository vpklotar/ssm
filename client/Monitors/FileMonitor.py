"""Tail a file and handle all new text."""
import threading
import time


class FileMonitor(object):
    """Tail a file and handle all new data written to it."""

    def __init__(self, main, config):
        """Initialize and setup the FileMonitor."""
        self.cfg = config
        self.main = main
        self.load_properties()
        self.start()

    def load_properties(self):
        """Load required properties."""
        self.path = self.cfg.get_required("location",
                                          "A file locations is required")
        self.sender = self.cfg.get_required("sender", "Sender(s) is required")

    def start(self):
        """Start the tailing of the file."""
        thread = threading.Thread(target=self.read)
        thread.daemon = True
        thread.start()

    def read(self):
        """Tail the file."""
        # TODO: Remember where to start reading
        file_handle = open(self.path, 'r')
        self.main.log("Monitoring " + self.path)
        while True:
            line = file_handle.readline()
            if not line:
                time.sleep(1)
                continue
            self.main.send(self.sender, line.strip())
