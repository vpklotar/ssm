"""Class for handling systemds journalctl."""
import threading
import select
import re
import os
from systemd import journal
from Monitors.ClientMonitor import ClientMonitor


class JournalctlMonitor(ClientMonitor):
    """Monitor for journalctl."""

    def __init__(self, main, config):
        """Initialize and start up the monitor."""
        self.cfg = config
        self.main = main
        self.load_config_variables()
        self.start()

    def load_config_variables(self):
        """Load all values  and/or set default values."""
        self.timeformat = self.cfg.get_default("timeformat", "%b %m %H:%S:%M")
        self.stringformat = self.cfg.get_default("stringformat",
                                                 "%DATETIME% %_HOSTNAME%\
                                                  %SYSLOG_IDENTIFIER%:\
                                                  %MESSAGE%")
        self.this_boot = self.cfg.get_default("thisboot", True)
        self.this_machine = self.cfg.get_default("thismachine", True)

        # Find all words beween percentage-signs
        self.parts = re.findall(r'(?<=%)(.*?)(?=s*%)', self.stringformat)
        self.sender = self.cfg.get_required("sender", "Sender(s) is required")
        self.start_position = self.cfg.get_default("startposition", "tail")

        # Load all "mathces" and put them in a list
        self.regex = self.cfg.get_default("regex", "")
        if self.regex:
            if isinstance(self.regex, str):
                self.regex = [self.regex]
        else:
            self.regex = list()  # Empty list if empty string

    def start(self):
        """Will start the seperate thread for handlering journalctl."""
        thread = threading.Thread(target=self.read)
        thread.daemon = True
        thread.start()

    def format(self, entry):
        """Format the data into the requested format."""
        dtime = entry["__REALTIME_TIMESTAMP"].strftime(self.timeformat)
        formated = self.stringformat

        for part in self.parts:
            if part in entry:
                formated = formated.replace("%" + part + "%", entry[part])
            elif part == "DATETIME":
                formated = formated.replace("%" + part + "%", dtime)

        return formated

    def load_matches(self, journal):
        """Load the matches in the config."""
        # Does the "matches"-configuration section exist
        if "matches" in self.cfg.cfg:
            # Loop through all field-matches
            for field in self.cfg["matches"]:
                # Is the 'matches'-variable a list?
                if isinstance(self.cfg["matches"][field], list):
                    # Add a match per list-item
                    for match in self.cfg["matches"][field]:
                        journal.add_match(str(field) + "=" + str(match))
                else:
                    # It is not a list, add the match
                    journal.add_match(str(field) + "=" +
                                      self.cfg["matches"][field])

    def regex_match(self, message):
        """Do the message have a match."""
        if len(self.regex) == 0:
            return True

        for regex in self.regex:
            match = re.match(str(regex), str(message))
            if match:
                return True
        return False

    def read(self):
        """
        Read the journal for messages.

        This will read the journalctl log for messages and send them back to
        main for handling
        """
        j = journal.Reader()
        j.log_level(journal.LOG_DEBUG)
        if self.this_boot:
            j.this_boot()

        if self.this_machine:
            j.this_machine()

        if self.start_position == "tail":
            j.seek_tail()
        elif self.start_position == "remember":
            if os.path.isfile('cursor'):
                with open('cursor', 'r') as file_handler:
                    j.seek_cursor(file_handler.read())
            else:
                j.seek_tail()

        j.get_previous()

        # If matches is specified in the configuration, add them to the journal
        self.load_matches(j)

        poller = select.poll()

        journal_fd = j.fileno()
        poll_event_mask = j.get_events()
        poller.register(journal_fd, poll_event_mask)

        self.main.log("JournalCtl monitor initiated.")
        while True:
            if poller.poll(250):
                j.process()
                for entry in j:
                    # Do the message have a match
                    if not self.regex_match(entry["MESSAGE"]):
                        # The message is not supposed to be sent
                        continue
                    # Format the log according to the config
                    formated = self.format(entry)

                    if self.start_position == "remember":
                        # If the start position should be the last messages
                        # remember the last message
                        with open('cursor', 'w') as file_handler:
                            file_handler.write(entry['__CURSOR'])

                    self.main.send(self.sender, formated)  # Send the log
