#!/usr/bin/python3
"""Main binder and program of DynMon - Synchronized Syslog Manager"""
import time
import threading
import sys
from Config import Config


class Main:
    """Main class of DynMon - Synchronized Syslog Manager"""

    monitors = []  # monitors
    senders = {}  # senders
    log_lock = threading.Lock()  # Print lock

    def __init__(self):
        """Initialize and start the whole program."""
        self.load_config()
        self.load_senders()
        self.load_monitors()

    def load_config(self):
        """Load the client configuration."""
        config_file = open('config.json')
        config_string = config_file.read()
        self.cfg = Config(config_string, self)

    def load_monitors(self):
        """Load and initilize all monitors specified in config."""
        monitoring_cfg = self.cfg.get_config("monitoring")
        for monitor in monitoring_cfg:
            cls = dynamically_load_class("Monitors." + monitor["type"] +
                                         "Monitor")
            inst = cls(self, Config(monitor, self))
            self.monitors.append(inst)

    def load_senders(self):
        """Load and initilize all senders specified in config."""
        servers_cfg = self.cfg.get_config("servers")
        for name in servers_cfg.cfg:
            server_cfg = servers_cfg.get_config(name)
            cls = dynamically_load_class("Sender." + server_cfg["type"] +
                                         "Sender")
            inst = cls(name, server_cfg, self)
            self.senders[name] = inst

    def send(self, senders, message):
        """Handle messages from monitor to sender."""
        if isinstance(senders, str):
            self.senders[senders].send(message)
        elif isinstance(senders, list):
            for sender in senders:
                self.senders[sender].send(message)

    def log(self, message):
        """Log a message to the console."""
        with self.log_lock:
            sys.stdout.write(message + "\n")
            sys.stdout.flush()


def dynamically_load_class(name):
    """Load a class dynamically."""
    dname = name.rfind(".")
    classname = name[dname+1:len(name)]
    module = __import__(name[0:dname], globals(), locals(), [classname])
    module = getattr(module, classname)
    cls = getattr(module, classname)
    return cls


MAIN_INSTANCE = Main()
while True:
    time.sleep(60)  # Run the script forever
