"""OpenSSL sender and encrypter."""
import threading
import subprocess
import queue
from Helpers.UDPHelper import UDPHelper
from Helpers.TCPHelper import TCPHelper


class OpenSSLSender(object):
    """Handle, encrypt and send messages via OpenSSL-RSA."""

    def __init__(self, name, config, main):
        """Initialize and setup OpenSSL-Sender."""
        self.name = name
        self.cfg = config
        self.main = main
        self.load_properties()
        self.queue = queue.Queue()

        # Start the thread responsible for sending
        self.encrypt_thread = threading.Thread(target=self.send_thread_func)
        self.encrypt_thread.daemon = True
        self.encrypt_thread.start()

    def load_properties(self):
        """Will load all the required properties."""
        self.address = self.cfg.get_required("ip", "No IP specified")
        self.port = self.cfg.get_required("port", "No port specified")
        self.transport = self.cfg.get_default("transport", "udp").lower()
        self.key = self.cfg.get_required("key", "No encryption key specified")

        if self.transport == "udp":
            self.helper = UDPHelper(self.address, self.port, self.main)
        elif self.transport == "tcp":
            self.helper = TCPHelper(self.address, self.port, self.main)
        else:
            print("transport is not UDP or TCP, will now quit")
            exit()

    def send(self, message):
        """Add a message to the send-queue."""
        self.queue.put(message)

    def send_thread_func(self):
        """Read messages from the send-queue, encrypt and send them."""
        while True:
            message = self.queue.get(True)
            encrypted = self.encrypt(message)
            self.helper.send(encrypted)

    def encrypt(self, data):
        """Use OpenSSL to encrypt a message."""
        process = subprocess.Popen(["openssl", "rsautl", "-encrypt",
                                    "-inkey", self.key, "-pubin"],
                                   stdout=subprocess.PIPE,
                                   stdin=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        utf8_data = bytes(data, "UTF-8")
        return process.communicate(input=utf8_data)[0]
