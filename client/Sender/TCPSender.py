"""Send messages over UDP."""
from Helpers.TCPHelper import TCPHelper


class TCPSender:
    """Handle and send messages over UDP."""

    def __init__(self, name, config, main):
        """Initialize and setup TCP-Sender."""
        self.name = name
        self.cfg = config
        self.main = main
        self.tcp_helper = TCPHelper(self.cfg["ip"],
                                    self.cfg["port"],
                                    self.main)

    def send(self, message):
        """Put a message in the send-queue."""
        self.tcp_helper.send(bytes(str(message), "UTF-8"))
