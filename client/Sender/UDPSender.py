"""Send messages over UDP."""
from Helpers.UDPHelper import UDPHelper


class UDPSender:
    """Handle and send messages over UDP."""

    def __init__(self, name, config, main):
        """Initialize and setup UDP-Sender."""
        self.name = name
        self.cfg = config
        self.main = main
        self.load_properties()

        self.udp_helper = UDPHelper(self.ip_address, self.port, self.main)

    def load_properties(self):
        """Load all required properties."""
        self.ip_address = self.cfg.get_required("ip", "An IP-adress for a \
                                                reciver is required")
        self.port = self.cfg.get_required("port", "A reciving port must be \
                                                   specifed")

    def send(self, message):
        """Put a message in the send-queue."""
        self.udp_helper.send(bytes(str(message), "UTF-8"))
