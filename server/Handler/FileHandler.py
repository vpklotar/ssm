"""Module that will write recived data to a file."""


class FileHandler(object):
    """Class that will write recived data to a file."""

    def __init__(self, name, config):
        """Initialize the FileHandler."""
        self.name = name
        self.cfg = config

    def send(self, message):
        """Will write the message to the file."""
        with open(self.cfg['location'], 'a') as file_handler:
            file_handler.write(message + "\n")
