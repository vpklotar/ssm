"""A helper module to handle communication with TCP."""

import socket
import queue
import threading


class TCPHelper(object):
    """This class will handle all TCP communication (client)."""

    THREADS = []
    BUFFER_SIZE = 2048

    def __init__(self, address, port, main):
        """Initialize and setup TCP-Helper."""
        self.address = address
        self.port = port
        self.main = main

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind((address, port))
        self.socket.listen(1)

        self.queue = queue.Queue()

        # Start the thread responsible for handleing connection status
        self.recive_thread = threading.Thread(target=self.recive_thread_func)
        self.recive_thread.daemon = True
        self.recive_thread.start()

    def append(self, data):
        """Will enqueue data to be sent."""
        self.queue.put(data)

    def get(self, block=True):
        """Get an item from the queue."""
        return self.queue.get(block)

    def handle_conn_thread(self, connection, address):
        """Will handle a TCP-connection and recive the data."""
        self.main.log('Connection from ' + str(address) + " was established.")
        while True:
            data = connection.recv(self.BUFFER_SIZE)
            if data:
                self.append(data)
            else:
                self.main.log('Connection to ' + str(address) + " was closed.")
                break  # The connection was broken

    def recive_thread_func(self):
        """Thread function to handle all sending."""
        while True:
            connection, address = self.socket.accept()
            # Star the thread responsible for this specefic accepted connection
            handle_thread = threading.Thread(target=self.handle_conn_thread,
                                             args=[connection, address])
            handle_thread.daemon = True
            handle_thread.start()
            self.THREADS.append(handle_thread)
