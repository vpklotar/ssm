"""A helper module to handle communication with UDP."""

import socket
import queue
import threading


class UDPHelper(object):
    """This class will handle all UDP communication (client)."""

    def __init__(self, address, port, main):
        """Initialize and setup UDP-Helper."""
        self.address = address
        self.port = port
        self.main = main

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind((address, port))
        self.queue = queue.Queue()

        # Start the thread responsible for reciving
        self.send_thread = threading.Thread(target=self.send_thread_function)
        self.send_thread.daemon = True
        self.send_thread.start()

    def append(self, data):
        """Append data to the reciving queue."""
        self.queue.put(data)

    def get(self, block=True):
        """Get a object in the queue."""
        return self.queue.get(block)

    def send_thread_function(self):
        """Thread function to handle all sending."""
        while True:
            data, addr = self.socket.recvfrom(1024)
            self.append(data)
