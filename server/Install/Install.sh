#!/bin/bash

if [ "$(id -u)" != "0" ]; then
  echo "Only root can run this script" 1>&2
  exit 1
fi

currentDir=`pwd`
parentDir=${currentDir::-8}

cp $currentDir/SystemDScript /etc/systemd/system/ssmserver.service
sed -ie "s:\$PWD:$parentDir:g" /etc/systemd/system/ssmserver.service
sed -ie "s:\$FILE_LOCATION:$parentDir/SSMServer.py:g" /etc/systemd/system/ssmserver.service

echo "Installed sucessfully"
