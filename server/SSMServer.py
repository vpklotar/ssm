#!/usr/bin/python3
"""Main program of """
import time
import threading
import sys
from Config import Config


class Main(object):
    """Main class to handle the main functionallity of the client."""

    handlers = {}
    servers = []
    log_lock = threading.Lock()

    def __init__(self):
        """Initialize and start the server."""
        self.load_config()
        self.load_servers()
        self.load_handlers()

    def load_config(self):
        """Load the client configuration."""
        config_file = open('config.json')
        config_string = config_file.read()
        self.cfg = Config(config_string, self)

    def load_handlers(self):
        """Load all the handlers."""
        for name in self.cfg["handlers"]:
            cfg = Config(self.cfg["handlers"][name], self)
            handler_type = str(cfg.get_required("type", "A type is required"))
            cls = dynamically_load_class("Handler." + handler_type + "Handler")
            inst = cls(name, cfg)
            self.handlers[name] = inst

    def load_servers(self):
        """Load all the servers."""
        for server_cfg in self.cfg["servers"]:
            scfg = Config(server_cfg, self)
            server_type = scfg.get_required("type", "A type is required")
            cls = dynamically_load_class("Server." + server_type + "Server")
            inst = cls(self, scfg)
            self.servers.append(inst)

    def send(self, handlers, message):
        """Distribute messages."""
        if isinstance(handlers, str):
            self.handlers[handlers].send(message)
        elif isinstance(handlers, list):
            for handler in handlers:
                self.handlers[handler].send(message)

    def log(self, message):
        """Log a message to the console."""
        with self.log_lock:
            sys.stdout.write(message + "\n")
            sys.stdout.flush()


def dynamically_load_class(name):
    """Dynamically load a class."""
    dname = name.rfind(".")
    classname = name[dname+1:len(name)]
    module = __import__(name[0:dname], globals(), locals(), [classname])
    module = getattr(module, classname)
    cls = getattr(module, classname)
    return cls


MAIN_INSTANCE = Main()
while True:
    time.sleep(60)  # Run the script forever
