"""Module to recive OpenSSL-RSA encrypted messages."""
import threading
import subprocess
from Helpers.UDPHelper import UDPHelper
from Helpers.TCPHelper import TCPHelper


class OpenSSLServer(object):
    """Class to recive OpenSSL-RSA encrypted messages."""

    def __init__(self, main, config):
        """Initialize and setup OpenSSL-Server."""
        self.cfg = config
        self.main = main
        self.load_properties()

        # Start the thread responsible for sending
        self.recive_thread = threading.Thread(target=self.recive_thread_func)
        self.recive_thread.daemon = True
        self.recive_thread.start()

    def load_properties(self):
        """Will load all the properties needed."""
        self.bind = self.cfg.get_default("bind", "0.0.0.0")
        self.port = self.cfg.get_required("port", "You must specify a port")
        self.transport = self.cfg.get_default("transport", "udp").lower()

        if self.transport == "udp":
            self.helper = UDPHelper(self.bind, self.port, self.main)
        elif self.transport == "tcp":
            self.helper = TCPHelper(self.bind, self.port, self.main)
        else:
            print("transport is not UDP or TCP, will now quit")
            exit()

    def recive_thread_func(self):
        """Listen for, recive and decrypt messages."""
        self.main.log("OpenSSL-server listening on (" + self.transport + ") " +
                      str(self.bind) + ":" + str(self.port))
        while True:
            data = self.helper.get()
            message = self.decrypt(data)
            if message:  # A length of zero indicates a SSL-error
                self.main.send(self.cfg["handler"], message.decode("UTF-8"))

    def decrypt(self, data):
        """Decrypt a openssl message."""
        process = subprocess.Popen(["openssl", "rsautl", "-decrypt", "-inkey",
                                    self.cfg["key"]],
                                   stdout=subprocess.PIPE,
                                   stdin=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
        return process.communicate(input=data)[0]
