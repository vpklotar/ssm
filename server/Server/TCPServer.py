"""Module to recive messages via UDP."""
import threading
from Helpers.TCPHelper import TCPHelper


class TCPServer(object):
    """Class to recive messages via UDP."""

    def __init__(self, main, config):
        """Initialize and setup TCP-Server."""
        self.cfg = config
        self.main = main
        self.load_properties()

        self.tcp_helper = TCPHelper(self.bind, self.port, main)

        # Start the thread responsible for sending
        self.recive_thread = threading.Thread(target=self.recive_thread_func)
        self.recive_thread.daemon = True
        self.recive_thread.start()

    def load_properties(self):
        """Load all required properties."""
        self.bind = self.cfg.get_default("bind", "0.0.0.0")
        self.port = self.cfg.get_required("port", "A port number must be specifed")  # noqa: E501
        self.handler = self.cfg.get_required("handler", "A handler must be specifed")  # noqa: E501

    def recive_thread_func(self):
        """Revice and handle data."""
        self.main.log("TCPServer listening on " + self.bind + ":" +
                      str(self.port))
        while True:
            self.main.send(self.handler, bytes.decode(self.tcp_helper.get(),
                                                      "UTF-8"))
