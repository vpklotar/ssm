"""Module to recive and print messages to the console(UDP)."""
import threading
import socket


class UDPPrinterServer(object):
    """Module to recive and print messages to the console(UDP)."""

    def __init__(self, main, config):
        """."""
        self.cfg = config
        self.main = main

        # Bind the reciving interface
        self.reciver = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.reciver.bind((config["bind"], config["port"]))

        # Start the thread responsible for sending
        self.recive_thread = threading.Thread(target=self.recive_thread_func)
        self.recive_thread.daemon = True
        self.recive_thread.start()

    def recive_thread_func(self):
        """Revice and print messages via UDP."""
        self.main.log("UDPPrinterServer listening on " + self.cfg["bind"] +
                      ":" + str(self.cfg["port"]))
        while True:
            data, addr = self.reciver.recvfrom(1024)
            message = data.decode("UTF-8")
            print(message)
